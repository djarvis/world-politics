/**
 * MIT License
 *
 * Copyright 2014 White Magic Software, Inc.
 */

/*jslint browser: true, devel: true, white: true, unparam: true */
/*global $, jQuery*/

"use strict";

$(document).ready( function() {
  $.getScript( "/js/columns.js", function() {
    var $current;

    $("div.columns").millerColumns({
      current: function( $item ) {
        $current = $item;
      }
    });

    /** Used to initialize code blocks called when a link is clicked. */
    function update( link, def, undef ) {
      $(link).on( "click", function( event ) {
        event.preventDefault();

        ($current === undefined) ?
          typeof undef === "function" && undef() :
          typeof def === "function" && def();

        return false;
      });
    }

    update( "#new",
      function() {
        console.log( "new child of: " + $current.attr( "id" ) );
      },
      function() {
        console.log( "new root sibling" );
      }
    );

    update( "#edit",
      function() {
        console.log( "edit: " + $current.attr( "id" ) );
      }
    );

    update( "#del",
      function() {
        console.log( "del: " + $current.attr( "id" ) );
      }
    );

    update( "#flag",
      function() {
        console.log( "flag: " + $current.attr( "id" ) );
      }
    );
  });
});

