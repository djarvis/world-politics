/**
 * MIT License
 *
 * Copyright 2015 White Magic Software, Ltd.
 */
package to.discuss;

import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.servlet.ServletContext;

/**
 * Abstracts opening a resource. Used by both Main and App.
 */
public interface Base {
  public final static String FILE_SEP = System.getProperty( "file.separator" );

  /**
   * Opens a file using the servlet context.
   */
  public default InputStream open( ServletContext context, String filename )
    throws FileNotFoundException {
    String f = FILE_SEP + filename;
    return validate( context.getResourceAsStream( f ), f );
  }

  /**
   * Checks to see if the input stream, which must correspond to the given
   * filename, is null.
   *
   * @param in The input stream to check against null.
   * @param filename The filename corresponding to the given input stream.
   *
   * @return The given input stream paramter, never null.
   * @throws FileNotFoundException if the given stream is null.
   */
  public default InputStream validate( InputStream in, String filename )
    throws FileNotFoundException {
    if( in == null ) {
      throw new FileNotFoundException( filename );
    }

    return in;
  }
}

