/**
 * MIT License
 *
 * Copyright 2015 White Magic Software, Ltd.
 */
package to.discuss.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;

/**
 * Invoked when the user attempts to load any page that requires verified
 * account credentials.
 */
public class Login extends App {
  /**
   * Called when the user submits an account name and password.
   */
  @Override
  protected void doPost(
    HttpServletRequest request,
    HttpServletResponse response ) throws ServletException {

    Subject user = SecurityUtils.getSubject();

    if( !user.isAuthenticated() ) {
      try {
        user.login( getToken( request ) );

        SavedRequest saved = WebUtils.getAndClearSavedRequest( request );

        if( saved != null ) {
          response.sendRedirect( saved.getRequestUrl() );
        }
      }
      catch( AuthenticationException ae ) {
        // Prompt the user to log in again.
        super.doGet( request, response );
      }
      catch( Exception e ) {
        throw new ServletException( e );
      }
    }
  }

  /**
   * Returns the authentication token used to verify that the user's
   * credentials are valid.
   *
   * @param request The request containing account name and password.
   */
  protected AuthenticationToken getToken( HttpServletRequest request )
    throws ServletException {
    return new UsernamePasswordToken(
      getAccount( request ),
      getPassword( request ) );
  }

  /**
   * Returns the value of the account.
   *
   * @return A non-null string.
   */
  protected String getAccount( HttpServletRequest request ) {
    return getParameter( request, "account" );
  }

  /**
   * Returns the value of the password (must be sent over HTTPS).
   *
   * @return A non-null string.
   */
  protected String getPassword( HttpServletRequest request ) {
    return getParameter( request, "password" );
  }
}

