/**
 * MIT License
 *
 * Copyright 2015 White Magic Software, Ltd.
 */
package to.discuss.servlet;

/**
 * Main HTTP request handler for /app/new. This class requires a context
 * to determine the correct "new" object to create.
 */
public class New extends App {
  public New() {
  }
}

