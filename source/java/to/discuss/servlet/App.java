/**
 * MIT License
 *
 * Copyright 2015 White Magic Software, Ltd.
 */
package to.discuss.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Include the status codes.
import static javax.servlet.http.HttpServletResponse.*;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import to.discuss.Base;
import to.discuss.util.AppURIResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main HTTP request handler for /app.
 */
public abstract class App extends HttpServlet implements Base {
  protected static final Logger LOG = LoggerFactory.getLogger( App.class );

  public App() {
  }

  protected void doGet( HttpServletRequest request,
                        HttpServletResponse response ) throws ServletException
  {
    try {
      if( preprocess( request, response ) ) {
        sendHeader( response );
        sendContent( response );
      }
    }
    catch( Exception e ) {
      throw new ServletException( e );
    }
    finally {
      // Must be called even if preprocess throws an exception.
      postprocess( request, response );
    }
  }

  /**
   * The default behaviour is to call doGet.
   */
  protected void doPost( HttpServletRequest request,
                         HttpServletResponse response ) throws ServletException
  {
    this.doGet( request, response );
  }

  /**
   * Called before the header is sent, but after the request and response
   * variable have been set. This will return false to indicate that
   * content should not be sent (usually in the case of a redirect).
   *
   * @return true Send the header and content (via XSL transformation).
   * @return false Redirect to another page.
   */
  protected boolean preprocess(
    HttpServletRequest request,
    HttpServletResponse response ) throws ServletException {
    return true;
  }

  /**
   * Called after the content is sent. This is guaranteed to be called, even
   * if the servlet throws an exception during preprocessing.
   */
  protected void postprocess(
    HttpServletRequest request,
    HttpServletResponse response ) throws ServletException {
  }

  /**
   * Writes the header information and response status (200 OK).
   */
  protected void sendHeader( HttpServletResponse response ) throws Exception {
    response.setContentType( getContentType() );
    response.setStatus( SC_OK );
  }

  protected void redirect( HttpServletResponse response ) throws Exception {
    response.sendRedirect( getView() );
  }

  /**
   * Returns the default content type.
   *
   * @return "text/html"
   */
  protected String getContentType() {
    return "text/html";
  }

  /**
   * Returns the default character encoding.
   *
   * @return "utf-8"
   */
  protected String getEncoding() {
    return "utf-8";
  }

  /**
   * Retrieves the document requested by the user and transmits the HTML
   * content to the client.
   */
  private void sendContent( HttpServletResponse response ) throws Exception {
    Transformer transformer = getTransformer();
    setParameters( transformer );
    transformer.setOutputProperty( OutputKeys.ENCODING, getEncoding() );
    transformer.transform( getDocument(), getResultStream( response ) );
  }

  protected void setParameters( Transformer transformer ) {
    transformer.setParameter( "P_AUTHENTICATED", isAuthenticated() );
  }

  /**
   * Answers whether the current user has been authenticated.
   *
   * @return true The current account is authenticated.
   */
  protected boolean isAuthenticated() {
    return SecurityUtils.getSubject().isAuthenticated();
  }

  /**
   * Converts the HTTP response output stream to a Result object.
   */
  protected Result getResultStream( HttpServletResponse response )
    throws IOException {
    return new StreamResult( response.getOutputStream() );
  }

  /**
   * Returns the XML document to transform into a web page.
   */
  protected Source getDocument() throws Exception {
    return openSource( getDocumentFilename() );
  }

  protected Source openSource( String filename ) throws IOException {
    return new StreamSource( open( filename ) );
  }

  private String getDocumentFilename() {
    return String.format( "database%s%s.xml", FILE_SEP, getAppName() );
  }

  protected String getStylesheetFilename() {
    return "common.xsl";
  }

  private Source getStylesheet() throws Exception {
    return openSource( getStylesheetFilename() );
  }

  /**
   * Returns the transformer factory used to obtain a transformer engine.
   * The factory has a custom URI resolver necessary to dereference the
   * "template://discuss.to/app" HREF path.
   */
  protected TransformerFactory getTransformerFactory() {
    TransformerFactory factory = TransformerFactory.newInstance();
    factory.setURIResolver( new AppURIResolver( this ) );

    return factory;
  }

  /**
   * Returns the XSL transformer engine.
   */
  protected Transformer getTransformer() throws Exception {
    return getTransformerFactory().newTransformer( getStylesheet() );
  }

  /**
   * Returns the app name, in lowercase.
   *
   * @return The application name without slashes.
   */
  public String getAppName() {
    return getClass().getSimpleName().toLowerCase();
  }

  /**
   * Returns the view to this servlet.
   *
   * @return "/app/classname" (e.g., "/app/home").
   */
  protected String getView() {
    return "/app/" + getAppName();
  }

  /**
   * Returns the value of the given parameter, or the empty string if no
   * such parameter exists.
   *
   * @return A non-null string.
   */
  protected String getParameter( HttpServletRequest request, String param ) {
    String result = request.getParameter( param );
    return result == null ? "" : result;
  }

  /**
   * Requests the servlet context to open a file. The servlet context
   * should be configured with the directories to search for the given
   * filename. The filename must start with a forward slash.
   */
  public InputStream open( String filename ) throws IOException {
    return open( getServletContext(), filename );
  }
}

