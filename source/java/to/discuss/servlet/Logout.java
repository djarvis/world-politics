/**
 * MIT License
 *
 * Copyright 2015 White Magic Software, Ltd.
 */
package to.discuss.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * Main HTTP request handler for /app/logout.
 */
public class Logout extends App {
  public Logout() {
  }

  /**
   * Log the user out and redirect back to the home page.
   */
  @Override
  protected boolean preprocess(
    HttpServletRequest request,
    HttpServletResponse response ) throws ServletException {

    try {
      SecurityUtils.getSubject().logout();
      (new Home()).redirect( response );
    }
    catch( Exception e ) {
      throw new ServletException( e );
    }

    return false;
  }
}

