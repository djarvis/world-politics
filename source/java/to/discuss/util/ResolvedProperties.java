/**
 * MIT License
 *
 * Copyright 2015 White Magic Software, Ltd.
 */
package to.discuss.util;

import java.io.IOException;
import java.io.InputStream;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Stack;

/**
 * Extends java.util.Properties to recursively parse ${property} values. This
 * allows property files to be self-referential. For example:
 * <pre>
 * model.language=en
 * model.path=/usr/local/app
 * model.base=${model.language}/${model.path}/
 * </pre>
 *
 * The value for <code>model.base</code>, when retrieved, will be equal to
 * <code>/usr/local/app/en/</code>.
 */
@SuppressWarnings("serial")
public class ResolvedProperties extends Properties {
  /**
   * Default constructor.
   */
  public ResolvedProperties() {
  }

  /**
   * Calls load to initialize the properties.
   *
   * @param inputStream A list of name=value pairs, one per line.
   */
  public ResolvedProperties( InputStream inputStream ) throws IOException {
    load( inputStream );
  }
  
  /**
   * Delegates to parent.
   *
   * @param defaults - The default property values.
   */
  public ResolvedProperties( Properties defaults ) {
    super( defaults );
  }
  
  @Override
  public String getProperty( String name ) {
    String value = super.getProperty( name );

    return value != null ? resolve( value ) : null;
  }
  
  @Override
  public String getProperty( String name, String defaultValue ) {
    // Call this.getProperty(String) to prevent a stack overflow.
    String value = this.getProperty( name );

    return value != null ? value : defaultValue;
  }
  
  /**
   * This will throw an IllegalArgumentException if there are mismatched
   * braces.
   *
   * @param s - The string with zero or more ${} references to resolve.
   * @return The value of the string, with all substituted values made.
   */
  public String resolve( String s ) {
    StringBuffer sb = new StringBuffer( 256 );
    Stack<StringBuffer> stack = new Stack<StringBuffer>();
    int len = s.length();
  
    for( int i = 0; i < len; i++ ) {
      char c = s.charAt( i );
  
      switch( c ) {
        case '$': {
          if( i + 1 < len && s.charAt( i + 1 ) == '{' ) {
            stack.push( sb );
            sb = new StringBuffer( 256 );
            i++;
          }
          break;
        }
  
        case '}': {
          if( stack.isEmpty() ) {
            throw new IllegalArgumentException( "unexpected '}'" );
          }
  
          String name = sb.toString();
  
          sb = stack.pop();
          sb.append( super.getProperty( name, null ) );
          break;
        }
  
        default: {
          sb.append( c );
          break;
        }
      }
    }
  
    if( !stack.isEmpty() ) {
      throw new IllegalArgumentException( "missing '}'" );
    }
  
    return sb.toString();
  }

  /**
   * Adds all the properties into a map, which exposes the Collections
   * inteface methods.
   *
   * @return A non-null map.
   */
  public Map<String, String> asMap() {
    Map<String, String> result = new HashMap<String, String>();

    // Forces the properties to resolve relative references.
    for( String name : stringPropertyNames() ) {
      result.put( name, this.getProperty( name ) );
    }

    return result;
  }
}

