/**
 * MIT License
 *
 * Copyright 2015 White Magic Software, Ltd.
 */
package to.discuss.util;

import java.io.InputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import java.net.URI;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

import to.discuss.Base;
import to.discuss.servlet.App;

/**
 * Extends java.util.Properties to recursively parse ${property} values.
 */
public class AppURIResolver implements URIResolver, Base {
  private App app;

  /**
   * Constructs a resolver for applications.
   *
   * @param appName The name of the application to use as a path for URI
   * resolution.
   */
  public AppURIResolver( App app ) {
    setApp( app );
  }

  /**
   * Dynamically resolve the path to the include file based on the app name.
   */
  @Override
  public Source resolve( String href, String base )
    throws TransformerException {
    try {
      return isTemplate( new URI( href ) ) ? openTemplate() : openFile( href );
    }
    catch( Exception e ) {
      throw new TransformerException( e );
    }
  }

  /**
   * Answers with the URI points to a dynamic template to include into the
   * stylesheet.
   */
  protected boolean isTemplate( URI uri ) {
    return "template".equalsIgnoreCase( uri.getScheme() ) &&
           "discuss.to".equalsIgnoreCase( uri.getAuthority() );
  }

  /**
   * Guaranteed to open an XSL template that corresponds to the app name.
   * If no template is found, or the default template file is missing, this
   * will return a hard-coded string as a StreamSource.
   *
   * @return A valid StreamSource containing a stylesheet DOM.
   */
  private Source openTemplate() {
    Source result = null;

    try {
      result = new StreamSource( open( getStylesheetFilename() ) );
    }
    catch( IOException ioe ) {
      try {
        result = new StreamSource( open( "template.xsl" ) );
      }
      catch( Exception e ) {
        result = new StreamSource( getDefaultTemplate() );
      }
    }

    return result;
  }

  /**
   * Opens a stylesheet relative to the XSL directory.
   */
  private Source openFile( String href ) throws IOException {
    return new StreamSource( open( href ) );
  }

  private InputStream open( String filename ) throws IOException {
    return getApp().open( filename );
  }

  /**
   * If the file-based template cannot be found, then use a hard-coded,
   * fail-safe version.
   */
  private Reader getDefaultTemplate() {
    return new StringReader(
      "<?xml version='1.0' encoding='utf-8'?>" +
      "<xsl:stylesheet version='2.0' " +
      "xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>" +
      "</xsl:stylesheet>" );
  }

  /**
   * Resolves the filename to use based on the app name.
   */
  private String getStylesheetFilename() {
    return String.format( "%s.xsl", getAppName() );
  }

  protected String getAppName() {
    return getApp().getAppName();
  }

  private void setApp( App app ) {
    this.app = app;
  }

  private App getApp() {
    return this.app;
  }
}

