<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:variable name="APP_NAME">support</xsl:variable>

<xsl:variable name="artefacts">
<artefacts>
  <css>support</css>
  <js>editor.min</js>
  <js>support</js>
</artefacts>
</xsl:variable>

</xsl:stylesheet>
