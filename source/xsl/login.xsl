<?xml version="1.0" encoding="utf-8"?>
<!--
 | MIT License
 |
 | Copyright 2014 White Magic Software, Inc.
 +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="APP_NAME">login</xsl:variable>

<xsl:variable name="artefacts">
<artefacts>
  <css>login</css>
</artefacts>
</xsl:variable>

<xsl:template match="policy">
  <form id="login" method="post" action="{$P_CONTEXT}/login" autocomplete="off">
    <fieldset form="login" name="login">
      <input type="text" autocomplete="off" id="account" name="account"
        placeholder="Account"/><br/>
      <input type="password" autocomplete="off" id="password" name="password"
        placeholder="Password"/><br/>
      <button type="submit">Log In</button>
    </fieldset>
  </form>
</xsl:template>

</xsl:stylesheet>
