<?xml version="1.0" encoding="utf-8"?>
<!--
 | MIT License
 |
 | Copyright 2014 White Magic Software, Inc.
 +-->
<xsl:stylesheet version="2.0"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
  indent="no"
  method="html"
  doctype-system="about:legacy-compat"
  encoding="utf-8"/>

<xsl:strip-space elements="*"/>

<!-- Custom URI resolver imports the pertinent app stylesheet. -->
<xsl:include href="template://discuss.to/app"/>

<!-- Application context that responds to HTTP requests. -->
<xsl:param name="P_CONTEXT" select="'/app'"/>

<!-- Set to true if the user is authenticated. -->
<xsl:param name="P_AUTHENTICATED" select="false()"/>

<!-- Read the entire menu into an XML fragment variable. -->
<xsl:variable name="MENU">
  <xsl:copy-of select="document( 'menu/menu.xml' )"/>
</xsl:variable>

<!-- Create a menu index based on the unique menu IDs. -->
<xsl:key name="items" match="/menus/items/item" use="@id"/>

<xsl:template match="/">
<html>
  <head>
    <meta charset='utf-8'/>
    <title>Liberum Consilium: moderated, transparent deliberation</title>

    <xsl:apply-templates select="$artefacts" mode="css"/>

    <link rel='stylesheet' type='text/css'
      href='//fonts.googleapis.com/css?family=Open+Sans'/>
    <link rel='stylesheet' type='text/css'
      href='//fonts.googleapis.com/css?family=Montserrat'/>
  </head>
  <body>
    <xsl:apply-templates select="$MENU" mode="menu"/>
    <xsl:apply-templates/>
    <xsl:apply-templates select="$MENU" mode="menu"/>
    <xsl:apply-templates select="$artefacts" mode="js"/>
  </body>
</html>
</xsl:template>

<!-- Make the document complete with div elements and classes. -->
<xsl:template match="*">
  <div class="{local-name()}"><xsl:apply-templates select="node()|@*"/></div>
</xsl:template>

<!-- The 'id' attribute indicates a link. -->
<xsl:template match="*[@id]">
  <div class="{local-name()}"><a
    href="{$P_CONTEXT}/{local-name()}/{@id}"><xsl:apply-templates
      select="node()|*"/></a></div>
</xsl:template>

<!-- The 'video' attribute indicates an embedded video. -->
<xsl:template match="*[@url]">
<div class="{local-name()}">
  <iframe width="560" height="315" src="{@url}" frameborder="0"></iframe>
</div>
</xsl:template>

<!-- Retain the attributes (except if named "class"). -->
<xsl:template match="@*">
  <xsl:if test="name() != 'class'"><xsl:copy-of select="."/></xsl:if>
</xsl:template>

<xsl:template match="menus" mode="menu">
  <!-- The application name is given by the imported stylesheet. -->
  <xsl:variable name="id" select="$MENU/menus/items/item[@link=$APP_NAME]/@id"/>

  <ul class="menu">
    <xsl:apply-templates select="menu[@id=$id]" mode="menu"/>
  </ul>
</xsl:template>

<xsl:template match="menu" mode="menu">
  <xsl:apply-templates mode="menu"/>
</xsl:template>

<!-- If the user is authenticated, then provide a log out link. -->
<xsl:template match="item[@id = '15']" mode="menu" priority="1">
  <xsl:if test="$P_AUTHENTICATED">
    <xsl:call-template name="menu-item"/>
  </xsl:if>
</xsl:template>

<xsl:template match="item" mode="menu">
  <xsl:call-template name="menu-item"/>
</xsl:template>

<xsl:template name="menu-item">
  <xsl:variable name="link" select="key( 'items', @id )/@link"/>
  <li class="item">
    <a href="{$P_CONTEXT}/{$link}" class="{$link}">
      <xsl:value-of select="key( 'items', @id )"/>
    </a>
  </li>
</xsl:template>

<xsl:template match="artefacts" mode="css">
  <xsl:apply-templates select="css"/>
</xsl:template>

<xsl:template match="artefacts" mode="js">
  <xsl:apply-templates select="js"/>
</xsl:template>

<xsl:template match="css">
  <link type='text/css' rel='stylesheet' href='/css/{.}.css'/>
</xsl:template>

<xsl:template match="js">
  <script type='text/javascript' src="/js/{.}.js"></script>
</xsl:template>

</xsl:stylesheet>
