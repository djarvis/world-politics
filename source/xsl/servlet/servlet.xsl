<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:output indent="yes" method="text" encoding="utf-8"/>

<xsl:strip-space elements="*"/>

<xsl:template match="/">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="menus">
  <xsl:apply-templates select="items"/>
</xsl:template>

<xsl:template match="items">
<xsl:result-document method="text" href="resources/servlet.properties">
<xsl:text># Machine generated: change via servlet.xsl.&#xa;</xsl:text>
<xsl:text>#&#xa;</xsl:text>
<xsl:text># classname = url path&#xa;</xsl:text>
<xsl:text>App=/app</xsl:text>
<xsl:text>&#xa;</xsl:text>
<xsl:apply-templates/>
</xsl:result-document>
</xsl:template>

<xsl:template match="item">
  <xsl:sequence
    select="concat(upper-case(substring(@link,1,1)),substring(@link, 2))"/>
  <xsl:text>=${App}/</xsl:text>
  <xsl:value-of select="@link"/>
  <xsl:text>&#xa;</xsl:text>
</xsl:template>

<!-- Ignore /menus/items -->
<xsl:template match="*"/>

</xsl:stylesheet>
