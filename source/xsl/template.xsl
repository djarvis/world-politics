<?xml version='1.0' encoding='utf-8'?>
<xsl:stylesheet version='2.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<!--
 | This should not be used because every menu item should have its own
 | corresponding page.
 +-->
<xsl:variable name="artefacts">
<artefacts></artefacts>
</xsl:variable>

</xsl:stylesheet>
