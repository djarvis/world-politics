<?xml version="1.0" encoding="utf-8"?>
<!--
 | MIT License
 |
 | Copyright 2014 White Magic Software, Inc.
 +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="APP_NAME">account</xsl:variable>

<xsl:variable name="artefacts">
<artefacts>
  <css>account</css>
</artefacts>
</xsl:variable>

</xsl:stylesheet>
