<?xml version="1.0" encoding="utf-8"?>
<!--
 | MIT License
 |
 | Copyright 2014 White Magic Software, Inc.
 +-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="APP_NAME">tags</xsl:variable>

<xsl:variable name="artefacts">
<artefacts>
  <css>tags</css>
  <js>jquery.min</js>
  <js>tags</js>
</artefacts>
</xsl:variable>

<!-- Override the match="*" from the common template by forcing priority. -->
<xsl:template match="taglist" priority="1">
  <div id="container">
    <div class="breadcrumb"></div>
    <!-- tabindex is required to register keypresses. -->
    <div class="columns" tabindex="1">
      <ul>
      <xsl:apply-templates mode="tag"/>
      </ul>
    </div>
    <div class="toolbar">
      <a id="new"  href="do.xhtml?action=tag&amp;command=new">new</a> |
      <a id="edit" href="do.xhtml?action=tag&amp;command=edit">edit</a> |
      <a id="del"  href="do.xhtml?action=tag&amp;command=delete">delete</a> |
      <a id="flag" href="do.xhtml?action=tag&amp;command=flag">flag</a>
    </div>
  </div>
  <div class="description">
    <blockquote>
    The <b>gas</b> tag refers to
    <a href="http://en.wikipedia.org/wiki/Natural_gas">natural gas</a>.
    </blockquote>
  </div>
</xsl:template>

<xsl:template match="tag" mode="tag">
<li>
  <xsl:attribute name="id">
    <xsl:value-of select="@id"/>
  </xsl:attribute>
  <xsl:value-of select="name"/>
  <xsl:if test="tag">
    <ul>
    <xsl:apply-templates select="tag" mode="tag"/>
    </ul>
  </xsl:if>
</li>
</xsl:template>

<!-- Ignore any nodes having nothing to do with tags. -->
<xsl:template match="*" mode="tag"/>

<!-- Ignore the tags not in the correct mode. -->
<xsl:template match="taglist"/>

</xsl:stylesheet>
