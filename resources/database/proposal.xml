<?xml version="1.0" encoding="utf-8"?>
<policy>
  <summary>
    <title>Wastewater Treatment Project</title>
  </summary>
  <synopsis>
    <paragraph>
      The proposed treatment facility will provide enhanced primary and
      secondary treatment for the region’s core area wastewater. Conveyance
      <xref id="0">system upgrades</xref> will link the facility to a new
      marine outfall at <xref id="1">McLoughlin Point</xref> and the
      <xref id="2">Resource Recovery Centre</xref>.
    </paragraph>
    <paragraph>
      The facility will use proven technology to ensure that long term
      operating and maintenance <xref id="3">costs</xref> are minimized.
      The new facility will be designed to include innovative wastewater
      treatment options. The administrative buildings will be built to
      <xref id="4">LEED Gold standards</xref>. Design and architectural
      guidelines have been incorporated into the <xref id="5">Request for
      Proposals</xref>, following input from the municipalities of
      Esquimalt and Victoria.
    </paragraph>
  </synopsis>
  <proposal>
    <activation>
      <date>Dec 01, 2016</date>
      <time>3:45pm</time>
    </activation>
    <parts>
      <part>
        <title>Site</title>
        <paragraph>
          The Treatment Plant will be located at <xref id="1">McLoughlin
          Point</xref>, in Esquimalt.
          
          The municipalities of Victoria, Saanich, Oak Bay, Esquimalt, View 
          Royal, Colwood and Langford collectively are participants of the 
          Core Area Liquid Waste Management Service which is managed and 
          operated by the Capital Regional District (CRD) in accordance with 
          the Core Area Liquid Waste Management Plan (CALWMP).
        </paragraph>
        <paragraph>
          The CRD is responsible to implement secondary wastewater treatment 
          area within timelines prescribed in the CALWMP, by the federally 
          legislated deadline of 2020. While the approved CALWMP continues 
          to identify McLoughlin Point as the location for the wastewater 
          treatment facility, in April 2014, the Township of Esquimalt 
          Council rejected the CRD’s revised rezoning application to locate 
          a wastewater treatment facility at McLoughlin Point.
        </paragraph>
        <sections>
          <section>
            <title>Architecture</title>
            <content>
              <paragraph>
                The design includes green roof, glass fronting with 
                harbour-front walkway. The design complies with the 
                architectural guidelines that were developed with input from 
                municipalities of Esquimalt and Victoria and approved by 
                Esquimalt council.
              </paragraph>
            </content>
          </section>
          <section>
            <title>Technical</title>
            <content>
              <paragraph>
                Facility technical requirements include:
                <list>
                  <item>Provide enhanced primary and secondary treatment 
                  incorporates high rate wet weather treatment, innovative 
                  moving bed bioreactor technology, biological aerated 
                  filtration and advanced oxidation.</item>
                  <item>Process will kill pathogens and reduce even further, 
                  pharmaceuticals and chemicals of concern as well as many 
                  other compounds found in wastewater.</item>
                  <item>Secondary treatment combined with the advanced 
                  oxidation process is capable of providing better treatment 
                  of pharmaceuticals and chemicals than tertiary membrane 
                  treatment.</item>
                </list>
              </paragraph>
            </content>
          </section>
          <section>
            <title>Capacity</title>
            <content>
              <paragraph>
                The winning proposal provided the CRD a facility with a 
                capacity of 124 million litres per day; this will handle 
                sewage generated until 2065 all within the affordability 
                ceiling for the project.
              </paragraph>
              <paragraph>
                This additional capacity would have saved the CRD $150M by 
                deferring the requirement for future additional plants.
              </paragraph>
            </content>
          </section>
        </sections>
      </part>
    </parts>
  </proposal>
</policy>
