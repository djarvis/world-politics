#!/bin/bash

# The purpose of this script is to generate installation documentation that
# is thematically consistent with the overview manual.

# Script's working directory.
DIR="$(cd "$( dirname "$0" )" && pwd)"

# Convert the install wiki (markdown) into a ConTeXt file.
pandoc \
  --data-dir=$DIR --template=template.tex \
  -t context -N --chapters \
  ../../wiki/Install.md > install.tex

# Generate PDF documentation from the ConTeXt file.
context --path=../common/ --purgeall install.tex

