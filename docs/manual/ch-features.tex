\startchapter[
  title={Features},
  reference=cha:introduction,
]
  \WPquote{A popular Government without popular information or the means of 
  acquiring it, is but a Prologue to a Farce or a Tragedy or perhaps both. 
  Knowledge will forever govern ignorance, and a people who mean to be their 
  own Governors must arm themselves with the power knowledge gives.}
  {James Madison}{Letter to William Barry}
  
  \startsection[title={Moderation}]
    Moderation is a way to decide the suitability of statements concerning a 
    \WPproposal{}, prior to inclusion and publication. Deliberation entails 
    discussing a \WPproposal's implications. Moderated deliberation, 
    then, is a public forum wherein all contributions are scrutinized before 
    being accepted.

    Without moderation, any information about a \WPproposal{} could be 
    submitted without regards to its veracity or appropriateness. Reliable 
    information is essential for any fair decision-making
    process.\cite[carpini@americans]
    
    Moderators are selected from the community.
  \stopsection

  \startsection[title={Reputation}]
    Reputation is a number that reflects the credibility of community 
    members. Credibility can help people gauge the subject matter expertise
    of a community member.\cite[aragones@reputation]
    
    Reputation controls how people may contribute. Since any registered 
    individual may suggest changes, a mechanism is required to prevent 
    vandalism and abuse. As reputation is earned, functionality that is 
    unavailable to low-reputation community members is
    unlocked.\cite[allahbakhsh@crowdsourcing]\WPcitesep{}\cite[adler@wikipedia]
    
    This also allows experts to be reliably identified after a month
    of participation.\cite[movshovitz@stackoverflow]
  \stopsection

  \startsection[title={Tags}]
    Tags classify subject matter in a hierarchical relationship. Tags are 
    used by community members to filter \WPproposals{} based on their 
    individual preferences. Some people have broad interests, while others 
    have a narrow focus. Assigning hierarchically organized tags to 
    \WPproposals{} enables flexible filtering and can improve search results.
    
    Tags (and synonyms) are added and structured by the community.
  \stopsection

  \startsection[title={History}]
    \WPname{} retains a publicly available record of all interactions for 
    every community member. The records are available for anyone to review 
    at any time. This may encourage openness, communication, and 
    accountability. A recorded history should also reduce incidents of
    vandalism and inappropriate content.\cite[farrar@policing]
  \stopsection

  \startsection[title={Transparency}]
    The complete
    \href{source code}{https://bitbucket.org/djarvis/world-politics/} is
    open for analysis and development.\cite[schryen@security]
  \stopsection

  \startsection[title={Registration}]
    A registrar is responsible for authorizing eligible voters. Assigning 
    the role of registrar is dependent upon the type of organization that 
    uses \WPname{}. For example, in municipal settings, the registrar could 
    be determined by City Hall.
    
    A registration teller is responsible for creating the credentials that 
    voters use for voting. This ensures that the voter can be authorized 
    when receiving anonymous credentials.\cite[clarkson@voting]
  \stopsection
  
  \startsection[title={Voting}]
    Goals for voting include correctness, verifiability, and anonymity.
    
    \startsubsection[title=Correctness]
      The system is deemed correct if:

      \startitemize
        \startitem
          only voters who have registered with the registrar may vote;
        \stopitem
        \startitem
          voters cannot vote multiple times on an item;
        \stopitem
        \startitem
          voters cannot replace votes;
        \stopitem
        \startitem
          voters may change how they voted on an item; and
        \stopitem
        \startitem
          third-parties cannot change the tabulation outcome.
        \stopitem
      \stopitemize
    \stopsubsection
    
    \startsubsection[title=Verifiability]
      Verifiability allows voters and observers to verify that the 
      outcomes correspond to the votes legitimately cast. Multiple 
      verifiability types can be distinguished,
      including:\cite[kremer@verify]
      
      \startitemize
        \startitem
          \bold{Individual.} Voters can verify that their votes are
          part of the tally and are counted in the final result.
        \stopitem
        \startitem
          \bold{Universal.} Anyone can verify that all votes have been
          tallied correctly, and voters can verify that their votes are
          included.
        \stopitem
        \startitem
          \bold{Eligible.} Anyone can verify that every vote for a
          particular outcome was cast by a registered voter, at most
          one vote per voter, and voters can verify that their own
          votes are legitimate.
        \stopitem
      \stopitemize
      
      Verifying results must be simple and expeditious.
    \stopsubsection
    
    \startsubsection[title=Anonymity]
      The system should further {\it{privacy}},
      {\it{coercion-resistance}} and
      {\it{receipt-freeness}}.\cite[delaune@coercion]
      
      \startitemize
        \startitem
          \bold{Privacy.} Votes must remain private, even when a
          third-party can view all system data, communications, and
          source code.
        \stopitem
        \startitem
          \bold{Coercion-resistance.} Voters cannot cooperate with a
          coercer to prove their vote's directionality (i.e.,
          {\it{for}} or {\it{against}}).
        \stopitem
        \startitem
          \bold{Receipt-freeness.} Voters must not receive any
          information (a receipt) that can prove the vote's
          directionality to a coercer.
        \stopitem
      \stopitemize
      
      Votes recieved from independent, remote locations (such as
      hand-held, Internet-enabled devices), are susceptible to
      coercion. The system does not, and arguably cannot, address this
      vulnerability.
    \stopsubsection
  \stopsection
\stopchapter
