#!/bin/bash

context --path=../common/ --purge manual.tex

# TODO: Fix the ConTeX file so it doesn't produce a superfluous first page.
pdftk manual.pdf cat 2-end output manual-trim.pdf
mv manual-trim.pdf manual.pdf

